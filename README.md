# Gunicorn Dockerfile

This docker-compose file will install Gunicorn on your server. A stand-alone web server with extensive functionality provided in a convenient way with the Flask framework for creating web applications in the Python programming language.

## Installation
>You need to have [Docker](https://docs.docker.com/engine/install/) and [Docker-compose](https://docs.docker.com/compose/install/) installed on your server.

Download the repository:
```sh
git clone https://gitlab.com/sewer01/gunicorn.git
```
# Usage Docker-compose
After the download, you need to go to the `gunicorn` catalog:
```sh
cd gunicorn
```
There will be a `files` directory with site configuration files * .py and `Dockerfile` and `docker-compose.yml`


Start server:
```sh
IMAGE_TAG = gunicorn docker-compose up --build -d
```

>IMAGE_TAG - name of the created image
>-d - start the container in the background.


>Note: the server runs on port 80 by default, and can be accessed from all IP addresses.

The server can be stopped and restarted by the commands:
```sh
docker-compose stop
docker-compose start
```
Stopping and removing a server:
```sh
docker-compose down --remove-orphans
```

>for a complete list of keys see https://docs.docker.com/engine/reference/commandline/compose_down/

# Usage Docker

If there is a need to start the server at a certain port, you can change `Dockerfile` and `docker-compose.yml` or use the commands:
```sh
docker build. -t `IMAGE_TAG`
docker run --rm -d -p `PORT`: 80 --name` NAME` `IMAGE_TAG` -b` HOST`: 80 server: app
```

> `IMAGE_TAG` is the name of the created image
> `PORT` - the desired free port on the server.
> `NAME` is the name of the container.
> `HOST` - interface and port for binding.

Server stop:
```sh
docker stop `NAME`
```
