# Server gunicorn
#
# VERSION 0.1
FROM python:3.6-alpine

WORKDIR /app
COPY requirements.txt .

# install gunicorn and flask
RUN pip install -r requirements.txt

# create user 'gunicorn'
RUN adduser -D gunicorn && chown -R gunicorn /app
USER gunicorn

# copying site files
COPY ./files .
EXPOSE 80
ENTRYPOINT ["gunicorn"]
CMD ["-b", "0.0.0.0:80", "server:app"]
